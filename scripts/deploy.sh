echo "Deploying app in $ENVIROMENT Server in"
echo "IP_SERVER: $IP_SERVER"
echo "APP_FOLDER: $APP_FOLDER"
echo "PM2_STARTER_FILE: $PM2_STARTER_FILE"

ssh  $SSH_USER@$IP_SERVER "mkdir -p ~/$APP_FOLDER/build"
scp  -r ./build $SSH_USER@$IP_SERVER:~/$APP_FOLDER
scp  -r ./$PM2_STARTER_FILE $SSH_USER@$IP_SERVER:~/$APP_FOLDER
ssh $SSH_USER@$IP_SERVER pm2 reload 23people-landing-Page

echo "Deployment finished in $ENVIROMENT :)"