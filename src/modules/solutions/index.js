import React from "react";
import pageBuilder from "../../utils/pageBuilder";
import data from "../../resources/solutions.js";

const Solutions = () => {
  const components = pageBuilder.build(data);
  return (
    <>
      {components.map(({ Component, props }) => (
        <Component {...props} />
      ))}
    </>
  );
};

export default Solutions;
