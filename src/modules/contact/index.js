import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { isEmail, isFullName, isChileanTelephone } from "../../validations";
import sendMail from "../../utils/mailer";
//import { loadArrayOfImages } from "../../utils/imageLoader";

import solutionsImg from '../../images/solutions.png'
import bigDataImg from '../../images/languages/big-data.jpg'
import wsImg from '../../images/languages/ws.jpg'
import bdImg from '../../images/languages/bd.jpg'
import frontendImg from '../../images/languages/frontend.jpg'
import cloudImg from '../../images/languages/cloud.jpg'
import mobileImg from '../../images/languages/mobile.jpg'
//import belenAlarconImg from '../../images/team/belen-alarcon.jpg'

import catalinaStangeImg from '../../images/hablemos/CatalinaStange.png'
import edisonAcunaImg from '../../images/hablemos/EdisonAcuna.png'

const TIME_TO_FADEOUT = 3000;
const Contact = () => {
  const [status, setStatus] = useState({ success: null, failed: null });
  const { register, handleSubmit, errors, reset } = useForm({
    mode: "onBlur",
  });
  const [canSend, setCanSend] = useState(!Object.keys(errors).length);
  const onSubmit = async (data) => {
    try {
      setCanSend(false);
      const result = await sendMail({ ...data, type: "comercial" });
      if (result.success) {
        setStatus({ failed: false, success: true });
        reset();
      } else {
        setStatus({ success: false, failed: true });
      }
    } catch (error) {
      setStatus({ success: false, failed: true });
    }
  };
  useEffect(() => {
    let timeOut;
    if (status.success || status.failed) {
      timeOut = setTimeout(() => {
        setStatus({ success: false, failed: false });
        setCanSend(true);
      }, TIME_TO_FADEOUT);
    }

    return () => clearTimeout(timeOut);
  }, [status.failed, status.success]);

  return (
    <div className="container-fluid contact">
      {status.success ? (
        <div className="contact-msj success">
          <h1>
            ¡Listo! eres parte de la mejora continua. Te estaremos contactando.{" "}
          </h1>
        </div>
      ) : null}
      {status.failed ? (
        <div class="contact-msj error">
          <h1>
            ¡Lo sentimos! tu mensaje no puede ser enviado. Por favor intenta de
            nuevo.
          </h1>
        </div>
      ) : null}

      <div className="row no-gutters contact-wrap">
        <div className="col-12 col-md-4 c-text">
          <h1>
            ¡Voilá! Enfócate en tus objetivos y nosotros nos encargamos del
            resto.
          </h1>
          <p>
            Ahora dinos, ¿Para qué somos buenos? Completa todos los campos del
            formulario y te responderemos lo más pronto posible.
          </p>
        </div>
        <div className="col-12 col-md-4 c-form">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className={`input-form ${errors.name ? "form-error" : ""}`}>
              <input
                type="text"
                id="name"
                name="name"
                ref={register({
                  required: "Por favor completa con tu Nombre",
                  validate: (input) => isFullName(input),
                })}
              />
              <label htmlFor="name">
                {errors.name ? errors.name.message : "Nombre Completo"}
              </label>
            </div>
            <div className={`input-form ${errors.email ? "form-error" : ""}`}>
              <input
                type="mail"
                id="email"
                name="email"
                ref={register({
                  required: "Por favor completa con tu Correo",
                  validate: (input) =>
                    isEmail(input) ||
                    "Ops, no creo poder procesar eso ¿puedes volver a revisar tus datos?",
                })}
              />
              <label htmlFor="email">
                {errors.email ? errors.email.message : "Correo"}
              </label>
            </div>
            <div className={`input-form ${errors.company ? "form-error" : ""}`}>
              <input
                type="text"
                id="company"
                name="company"
                ref={register({
                  required: "Por favor completa con tu Empresa",
                  validate: (input) => isFullName(input),
                })}
              />
              <label htmlFor="company">
                {errors.company ? errors.company.message : "Empresa"}
              </label>
            </div>
            <div
              className={`input-form ${errors.phoneNumber ? "form-error" : ""}`}
            >
              <input
                id="phoneNumber"
                name="phoneNumber"
                placeholder="+56 X XXXXXXXX"
                ref={register({
                  required: "Por favor completa con tu Teléfono",
                  lessThan: (value) =>
                    value.length < 10 ||
                    "Oops! El número de teléfono mínimo debe contener  10 caracteres. Por favor intenta de nuevo.",
                  validate: (input) => isChileanTelephone(input),
                })}
              />
              <label htmlFor="phoneNumber">
                {errors.phoneNumber ? errors.phoneNumber.message : "Teléfono"}
              </label>
            </div>
            <div
              className={`input-form input-textarea ${
                errors.message ? "form-error" : ""
              }`}
            >
              <textarea
                id="message"
                name="message"
                ref={register({
                  required: "Por favor completa con tu Mensaje",
                  maxLength: {
                    value: 150,
                    message:
                      "Oops! Se alcanzo el máximo de caracteres. Por favor intenta de nuevo.",
                  },
                  pattern: {
                    value: /[a-zñáéíóú0-9,.-_]+/im,
                    message:
                      "Oops! El mensaje contiene caracteres inválidos. Por favor intenta de nuevo.",
                  },
                })}
              ></textarea>
              <label htmlFor="message">
                {errors.message ? errors.message.message : "Mensaje"}
              </label>
            </div>
            <div className="input-buttons buttons-actions">
              <button type="submit" disabled={!canSend}>
                Enviar
              </button>
            </div>
          </form>
        </div>
        <div className="col-12 col-md-4 c-contacts">
          <div className="container-fluid template-wrapper cases">
            <div className="container-fluid template-wrapper cases">
                  <div className="col-12 col-md-12 col-lg-7 case-card">
                  <div class="row">

                    <div className="col-6 col-md-6 col-lg-6 case-card">
                      <img src={catalinaStangeImg} alt="Belen Alarcón" />
                    </div>
                    <div class="col-6 col-md-6 col-lg-6">
                        <b>Catalina Stange Salinas</b>
                        <br/>
                        Reclutamiento
                        <br/>
                        <a
                          alt="Linkedin Catalina"
                          href="https://www.linkedin.com/in/catalinapazstangesalinas/"
                          target="blank"
                        >
                          <i className="fab fa-linkedin"></i> LinkedIn
                        </a>
                    </div>
                  </div>
                </div>

                <div className="col-12 col-md-12 col-lg-7 case-card">
                  <div class="row">

                    <div className="col-6 col-md-6 col-lg-6 case-card">
                      <img src={edisonAcunaImg} alt="Belen Alarcón" />
                    </div>
                    <div class="col-6 col-md-6 col-lg-6">
                        <b>Edison Acuña Leiton</b>
                        <br/>
                        Comercial
                        <br/>
                        
                        <a
                          alt="Linkedin Edison"
                          href="https://www.linkedin.com/in/edisona1/"
                          target="blank"
                        >
                          <i className="fab fa-linkedin"></i> LinkedIn
                        </a>
                    </div>
                  </div>
                </div>
                

             
            </div>
          </div>
        </div>
      </div>

      <div className="container-fluid home solutions">
      <div>
        <div className="row no-gutters services">
          <div className="col-12 col-md-7 col-lg-4 order-1 order-md-2 serv-title">
            <h1>Siempre es un gusto poder ayudarte a mejorar las cosas</h1>
            <p>Siempre es importante captar el alcance real de tu desarrollo tecnológico, para que esto sea así,  te ofrecemos una  entrega constante de valor, vanguardia tecnológica, agilidad en su forma esencial;  sin dejar de lado una preocupación real por ti y nuestras personas.</p>
          </div>

          <div className="col-12 col-md-5 col-lg-8 col-xl-7 order-2 order-md-1 serv-img">
            <li key='1'>
                <img src={solutionsImg} alt="Solutions" />           
            </li>
          </div>
        </div>
      </div>
    </div>


    <div className="container-fluid">
        <div className="row no-gutters serv-subtitle white1">
          <div className="col">
            <h1>Con herramientas tecnológicas de alto nivel y seguridad</h1>
           
          </div>
        </div>
    </div>




    <div className='container-fluid clients'>
        <div className='row no-gutters'>
          <div className='col client-list white-clients'>
            <ul>
            
                <li key='1'>
                  <img src={bigDataImg} alt="Big Data" />
                </li>
                <li key='2'>
                  <img src={wsImg} alt="WS" />
                </li>
                <li key='3'>
                  <img src={bdImg} alt="BD" />
                </li>
                <li key='4'>
                  <img src={frontendImg} alt="Frontend" />
                </li>
                <li key='5'>
                  <img src={cloudImg} alt="Cloud" />
                </li>
                <li key='6'>
                  <img src={mobileImg} alt="Mobile" />
                </li>
              
            </ul>
          </div>
        </div>
    </div>










    </div>

    
  );
};

export default Contact;
