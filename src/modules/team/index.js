import React from "react";
import data from "../../resources/team.js";
import pageBuilder from "../../utils/pageBuilder";

const Team = () => {
  const components = pageBuilder.build(data);
  return (
    <>
      {components.map(({ Component, props }, i) => (
        <Component {...props}  key={`${i}team`}/>
      ))}
    </>
  );
};

export default Team;
