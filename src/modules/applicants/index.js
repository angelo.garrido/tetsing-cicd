import React, { useEffect } from "react";
import data from "../../resources/applicants.js";
import pageBuilder from "../../utils/pageBuilder";

const Applicants = () => {
  useEffect(() => {
    ancla()
  }, [])
  const ancla = () => {
    const URLhash = window.location.hash;
    if (URLhash === "#Beneficios") {
      setTimeout(() => {
        const top = document.querySelector(".Beneficios").scrollHeight;
        window.scrollTo(0, top);
      }, 500)
    }
  }


  const components = pageBuilder.build(data);
  return (
    <>
      {components.map(({ Component, props }, i) => (
        <Component {...props} key={`${i}applicants`} />
      ))}
    </>
  );
};

export default Applicants;
