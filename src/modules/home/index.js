import React from "react";
import pageBuilder from "../../utils/pageBuilder";
import data from "../../resources/home.js";

const Home = () => {
  let isShowModal = sessionStorage.getItem('isShowModal');
    if(isShowModal == null) {
      sessionStorage.setItem('isShowModal', true);
    }
  const components = pageBuilder.build(data);
  return (
    <div className="container-fluid home">
      {components.map(({ Component, props }, i) => (
        <Component {...props} key={`${i}home`}/>
      ))}
    </div>
  );
};

export default Home;
