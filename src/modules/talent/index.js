import React from "react";
import pageBuilder from "../../utils/pageBuilder";
import data from "../../resources/talent.js";

const Talent = () => {
  const components = pageBuilder.build(data);
  return (
    <>
      {components.map(({ Component, props }) => (
        <Component {...props} />
      ))}
    </>
  );
};

export default Talent;
