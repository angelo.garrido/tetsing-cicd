import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import translationEng from './locales/en/translation.json';
import translationEs from './locales/es/translation.json';

i18n.use(LanguageDetector).init({
  resources: {
    en: {
      translations: translationEng
    },
    es: {
      translations: translationEs
    }
  },
  fallbackLng: 'es',
  debug: true,

  ns: ['translations'],
  defaultNS: 'translations',

  keySeparator: false,

  interpolation: {
    escapeValue: false,
    formatSeparator: ','
  },

  react: {
    wait: true
  }
});

export default i18n;
