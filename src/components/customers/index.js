import React, { useState, useEffect } from "react";
import { loadArrayOfImages } from "../../utils/imageLoader";

const Customers = ({ images, altImg=null }) => {
  const [imgsToDisplay, setImgsToDisplay] = useState([]);

  useEffect(() => {
    async function loadImages() {
      const result = await loadArrayOfImages(images);
      setImgsToDisplay(result);
    }
    loadImages();
  }, [images]);

  return (
    <ul>
      {imgsToDisplay.map((img, i) => (
        <li key={i}>
          <img alt={altImg?altImg+"-"+i:""} src={img} />
        </li>
      ))}
    </ul>
  );
};

export default Customers;
