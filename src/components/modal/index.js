import React, { useState, useEffect } from 'react';
import { Modal,Box} from '@material-ui/core';
import { loadSingleImage } from "../../utils/imageLoader";


const BasicModal = ({ 
  
  className, 
  title, 
  body,
  id,
  button,
  img = "placeholder-img.jpg",
  altImg,
  classImg
}) => {
  const [open, setOpen] = useState(true);
  const [imgToDisplay, setImgToDisplay] = useState(null);

  useEffect(() => {
    if (sessionStorage.getItem('isShowModal') === "true") {
      sessionStorage.setItem('isShowModal', false);
    }else {
      setOpen(false)
    }
    const loadImages = async () => {
      const result = await loadSingleImage(img);
      setImgToDisplay(result);
    }
    loadImages();
  }, [img]);

  const handleClose = () => setOpen(!open);

  return (
    <Box flexWrap="wrap" id={id} className={className}>
      <Modal open={open} onClose={handleClose}>
          <Box m={5} 
              bgcolor="#2E398B" 
              minHeight="20vh"
              top={40} 
              p={2} 
              position='absolute' 
              zIndex='tooltip'
              justifyContent="center"

              >
            <Box fontWeight="fontWeightMedium" color="#D8BD6F">
            <h1>{title}</h1> 
            </Box>
            <Box fontWeight="fontWeightMedium" color="#FFFFFF" fontStyle="normal" fontSize={20} m={1}> 
            <p>{body}</p>
            <div className={classImg}>
              {imgToDisplay ? <img alt={altImg} src={imgToDisplay} /> : null}
            </div>
            </Box>
        </Box>
      </Modal>    
    </Box>
  );
};

export default BasicModal;
