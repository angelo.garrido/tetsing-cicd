import React, { useEffect, useState } from "react";

const MultiColorText = ({
  componentClass = "multiColorTitle",
  wordsWithColors = [],
  timeBetweenWord = 800,
}) => {
  const colors = wordsWithColors;
  const [word, setWord] = useState(
    wordsWithColors.length ? wordsWithColors[0] : {}
  );
  const [position, setPosition] = useState(0);
  useEffect(() => {
    const changeColor = (currentPosition = 0) => {
      setPosition(currentPosition);
      if (position < colors.length - 1) {
        setPosition(position + 1);
      } else {
        setPosition(0);
      }

      return setWord(colors[position]);
    };
    let timeOut;
    if (colors.length) {
      timeOut = setTimeout(() => changeColor(position), timeBetweenWord);
    }
    return () => clearTimeout(timeOut);
  }, [colors, position, timeBetweenWord]);

  return (
    <span className={componentClass} style={{ color: word.color }}>
      {word.text}
    </span>
  );
};

export default MultiColorText;
