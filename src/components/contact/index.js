import React from 'react';
import { useHistory } from 'react-router-dom';

const Contact = ({ className = 'container-fluid contact-split', firstColumn, secondColumn }) => {
  let history = useHistory();
  return (
    <div className={className}>
      <div className="row no-gutters">
        <div className={firstColumn.className}>
          {firstColumn.title ? <h4>{firstColumn.title}</h4> : null}
          {firstColumn.body ? <p>{firstColumn.body}</p> : null}
          <div className="buttons-actions">
            <button
              onClick={() =>
                firstColumn.button.isExternalLink
                  ? (window.location.href = firstColumn.button.url)
                  : history.push(firstColumn.button.url)
              }
            >
              {firstColumn.button.text}
            </button>
          </div>
        </div>
        <div className={secondColumn.className}>
          {secondColumn.title ? <h4>{secondColumn.title}</h4> : null}
          {secondColumn.body ? <p>{secondColumn.body}</p> : null}
          <div className="buttons-actions">
            <button
              onClick={() =>
                secondColumn.button.isExternalLink
                  ? (window.location.href = secondColumn.button.url)
                  : history.push(secondColumn.button.url)
              }
            >
              {secondColumn.button.text}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contact;
