import React from "react";
import CardHover from "../cardHover";


const MultiCardsHover = ({
  title,
  cards,
  subTitle,
  dropTitle,
  className = "container-fluid no-gutters template-wrapper white2",
  rowSubClass = "container"
}) => {
  return (
    <div className={className}>
      <div className={rowSubClass}>
        {title ? <h1>{title}</h1> : null}
        {subTitle ? <h2>{subTitle}</h2>: null}
        {dropTitle ? <p align="center">{dropTitle}</p>: null}
        <div className="row">
          {cards.map(({ ...props }, i) => (
            <CardHover {...props} key={`${i}multicard`} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default MultiCardsHover;
