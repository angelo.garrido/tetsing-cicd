import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { loadSingleImage } from "../../utils/imageLoader";

const Card = ({
  className,
  subTitle,
  altImg,
  body,
  img = "placeholder-img.jpg",
  id,
  hasButton,
  button,
}) => {
  let history = useHistory();
  const [imgToDisplay, setImgToDisplay] = useState(null);
  useEffect(() => {
    async function loadImages() {
      const result = await loadSingleImage(img);
      setImgToDisplay(result);
    }
    loadImages();
  }, [img]);

  return (
    <div id={id} className={className}>
      {imgToDisplay ? <img alt={altImg} src={imgToDisplay} /> : null}
      {subTitle ? <h5>{subTitle}</h5> : null}
      {body ? <p>{body}</p> : null}
      {hasButton ? (
        <div className="buttons-actions">
          <button
            onClick={() =>
              button.isExternalLink
                ? (window.location.href = button.url)
                : history.push(button.url)
            }
          >
            {button.text}
          </button>
          
        </div>
              ) : null}
      </div>
  );
};

export default Card; 
