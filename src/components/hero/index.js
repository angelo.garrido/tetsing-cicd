import React from 'react';
import { useHistory } from 'react-router-dom';

const Hero = ({ className = 'container-fluid contact-split', firstColumn, secondColumn, thirdColumn }) => {
  let history = useHistory();
  return (
    <div className={className}>
      <div className="row no-gutters">
        <div className={firstColumn.className}>
          {firstColumn.title ? <h4>{firstColumn.title}</h4> : null}
          {firstColumn.body ? <p>{firstColumn.body}</p> : null}
          <div className="buttons-actions">
            {firstColumn.buttons.map(buttonData => {
              return (
                <button
                  className={buttonData.className}
                  onClick={() =>
                    buttonData.isExternalLink
                      ? (window.location.href = buttonData.url)
                      : history.push(buttonData.url)
                  }
                >
                  {buttonData.text}
                </button>
              );
            })}
          </div>
        </div>
        <div className={secondColumn.className}>
          {secondColumn.title ? <h4>{secondColumn.title}</h4> : null}
          {secondColumn.body ? <p>{secondColumn.body}</p> : null}
          <div className="buttons-actions">
              {secondColumn.buttons.map(buttonData=>{
                 return (
                    <button
                      className={buttonData.className}
                      onClick={() =>
                        buttonData.isExternalLink
                          ? (window.location.href = buttonData.url)
                          : history.push(buttonData.url)
                      }
                    >
                      {buttonData.text}
                    </button>
                  );
                })}
          </div>
        </div>
        <div className={thirdColumn.className}>
          {thirdColumn.title ? <h4>{thirdColumn.title}</h4> : null}
          {thirdColumn.body ? <p>{thirdColumn.body}</p> : null}
          <div className="buttons-actions">
              {thirdColumn.buttons.map(buttonData=>{
                 return (
                    <button
                      className={buttonData.className}
                      onClick={() =>
                        buttonData.isExternalLink
                          ? (window.location.href = buttonData.url)
                          : history.push(buttonData.url)
                      }
                    >
                      {buttonData.text}
                    </button>
                  );
                })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hero;
