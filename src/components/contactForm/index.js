import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { isEmail, isFullName, isChileanTelephone } from "../../validations";
import sendMail from "../../utils/mailer";
import pageBuilder from "../../utils/pageBuilder";
import data from "../../resources/contact";




const TIME_TO_FADEOUT = 3000;
const ContactForm = () => {
  const components = pageBuilder.build(data);
  const [status, setStatus] = useState({ success: null, failed: null });
  const { register, handleSubmit, errors, reset } = useForm({
    mode: "onBlur",
  });
  const [canSend, setCanSend] = useState(!Object.keys(errors).length);
  const onSubmit = async (data) => {
    try {
      setCanSend(false);
      const result = await sendMail({ ...data, type: "comercial" });
      if (result.success) {
        setStatus({ failed: false, success: true });
        reset();
      } else {
        setStatus({ success: false, failed: true });
      }
    } catch (error) {
      setStatus({ success: false, failed: true });
    }
  };
  useEffect(() => {
    let timeOut;
    if (status.success || status.failed) {
      timeOut = setTimeout(() => {
        setStatus({ success: false, failed: false });
        setCanSend(true);
      }, TIME_TO_FADEOUT);
    }

    return () => clearTimeout(timeOut);
  }, [status.failed, status.success]);

  return (
    <div className="container-fluid contact">
      {status.success ? (
        <div className="contact-msj success">
          <h1>
            ¡Listo! eres parte de la mejora continua. Te estaremos contactando.{" "}
          </h1>
        </div>
      ) : null}
      {status.failed ? (
        <div class="contact-msj error">
          <h1>
            ¡Lo sentimos! tu mensaje no puede ser enviado. Por favor intenta de
            nuevo.
          </h1>
        </div>
      ) : null}

      <div className="row no-gutters contact-wrap">
        <div className="col-12 col-md-4 c-text">
          <h1>
            ¡Voilá! Enfócate en tus objetivos y nosotros nos encargamos del
            resto.
          </h1>
          <p>
            adfsddaaAhora dinos, ¿Para qué somos buenos? Completa todos los campos del
            formulario y te responderemos lo más pronto posible.
          </p>
        </div>
        <div className="col-12 col-md-8 c-form">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className={`input-form ${errors.name ? "form-error" : ""}`}>
              <input
                type="text"
                id="name"
                name="name"
                ref={register({
                  required: "Por favor completa con tu Nombre",
                  validate: (input) => isFullName(input),
                })}
              />
              <label htmlFor="name">
                {errors.name ? errors.name.message : "Nombre Completo"}
              </label>
            </div>
            <div className={`input-form ${errors.email ? "form-error" : ""}`}>
              <input
                type="mail"
                id="email"
                name="email"
                ref={register({
                  required: "Por favor completa con tu Correo",
                  validate: (input) =>
                    isEmail(input) ||
                    "Ops, no creo poder procesar eso ¿puedes volver a revisar tus datos?",
                })}
              />
              <label htmlFor="email">
                {errors.email ? errors.email.message : "Correo"}
              </label>
            </div>
            <div className={`input-form ${errors.company ? "form-error" : ""}`}>
              <input
                type="text"
                id="company"
                name="company"
                ref={register({
                  required: "Por favor completa con tu Empresa",
                  validate: (input) => isFullName(input),
                })}
              />
              <label htmlFor="company">
                {errors.company ? errors.company.message : "Empresa"}
              </label>
            </div>
            <div
              className={`input-form ${errors.phoneNumber ? "form-error" : ""}`}
            >
              <input
                id="phoneNumber"
                name="phoneNumber"
                placeholder="+56 X XXXXXXXX"
                ref={register({
                  required: "Por favor completa con tu Teléfono",
                  lessThan: (value) =>
                    value.length < 10 ||
                    "Oops! El número de teléfono mínimo debe contener  10 caracteres. Por favor intenta de nuevo.",
                  validate: (input) => isChileanTelephone(input),
                })}
              />
              <label htmlFor="phoneNumber">
                {errors.phoneNumber ? errors.phoneNumber.message : "Teléfono"}
              </label>
            </div>
            <div
              className={`input-form input-textarea ${
                errors.message ? "form-error" : ""
              }`}
            >
              <textarea
                id="message"
                name="message"
                ref={register({
                  required: "Por favor completa con tu Mensaje",
                  maxLength: {
                    value: 150,
                    message:
                      "Oops! Se alcanzo el máximo de caracteres. Por favor intenta de nuevo.",
                  },
                  pattern: {
                    value: /[a-zñáéíóú0-9,.-_]+/im,
                    message:
                      "Oops! El mensaje contiene caracteres inválidos. Por favor intenta de nuevo.",
                  },
                })}
              ></textarea>
              <label htmlFor="message">
                {errors.message ? errors.message.message : "Mensaje"}
              </label>
            </div>
            <div className="input-buttons buttons-actions">
              <button type="submit" disabled={!canSend}>
                Enviar
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ContactForm;



