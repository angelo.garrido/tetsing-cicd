import React from "react";

const Embed =  ({ 
  iframeUrl,
  className
  }) => {
  
    return (
      <div class={className}>
        <iframe src={iframeUrl} title="embed" width={1000} height={500} />
      </div>
    );
};

export default Embed;

