import React from "react";
import Card from "../card";

const MultiCards = ({
  title,
  cards,
  subTitle,
  dropTitle,
  className = "container-fluid no-gutters template-wrapper white2",
  rowSubClass = "container",
  anclaClass = null
}) => {
  return (
    <div className={className + ' ' +anclaClass}>
      <div className={rowSubClass}>
        {title ? <h3>{title}</h3> : null}
        {subTitle ? <h2>{subTitle}</h2>: null}
        {dropTitle ? <p align="center">{dropTitle}</p>: null}
        <div className="row">
          {cards.map(({ ...props }, i) => (
            <Card {...props} key={`${i}multicard`} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default MultiCards;
