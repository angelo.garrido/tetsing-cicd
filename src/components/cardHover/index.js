import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { loadSingleImage } from "../../utils/imageLoader";

const CardHover = ({
  className,
  subClassName,
  secondSubClassName,
  ImgClassName,
  subTitle,
  body,
  img = "placeholder-img.jpg",
  id,
  hasButton,
  button,
  link,
  linkClassName
}) => {
  let history = useHistory();
  const [imgToDisplay, setImgToDisplay] = useState(null);
  useEffect(() => {
    async function loadImages() {
      const result = await loadSingleImage(img);
      setImgToDisplay(result);
    }
    loadImages();
  }, [img]);

  return (
    <div id={id} className={className}>
      <div class={subClassName}>
          {imgToDisplay ? 
            <img alt="" src={imgToDisplay} className={ImgClassName} /> : null
          }
          <div className={secondSubClassName}>
            {subTitle ? <h2>{subTitle}</h2> : null}
            {body ? <p>{body}</p> : null}
            <a href={link} target="blank">
                <i className={linkClassName}></i>
              </a>
            {hasButton ? (
            <div className="buttons-actions">
              <button
                onClick={() =>
                  button.isExternalLink
                    ? (window.location.href = button.url)
                    : history.push(button.url)
                }
              >
                {button.text}
              </button>
              
              
            </div>
          
                  ) : null}
          </div>
      </div>
    </div>





  );
};

export default CardHover; 
