import React from "react";

const List = ({ id, length }) => {
  const list = [];

  for (let i = 0; i < length; i += 1) {
    list.push(
      <li
        key={`${id}${i}`}
        data-target={`#${id}`}
        data-slide-to={i}
        className={i === 0 ? "active" : ""}
      ></li>
    );
  }

  return <ol className="carousel-indicators">{list}</ol>;
};

export default List;
