import React from "react";

const Wrapper = ({ wrapperClass, children }) => {
  return <div className={wrapperClass}>{children}</div>;
};

export default Wrapper;
