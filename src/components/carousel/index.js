import React from 'react';
import Wrapper from './wrapper';
import List from './list';
import pageBuilder from '../../utils/pageBuilder';

const Carousel = ({ id, body }) => {
  const components = pageBuilder.build(body);
  return (
    <div id={id} className="carousel slide" data-ride="carousel">
      <List length={body.length} id={id} />
      <div className="carousel-inner">
        {components.map(({ Component, props: { wrapperClass, ...childrenProps } }, i) => (
          <Wrapper wrapperClass={wrapperClass} key={`${i}carouselWrapper`}>
            <Component {...childrenProps} />
          </Wrapper>
        ))}
      </div>
    </div>
  );
};

export default Carousel;
