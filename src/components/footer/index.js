import React, { useState, useEffect } from "react";
import { loadSingleImage } from "../../utils/imageLoader";

const Footer = ({ img = "logo.svg" }) => {
  const [imgToDisplay, setImgToDisplay] = useState(null);
  useEffect(() => {
    async function loadImages() {
      const result = await loadSingleImage(img);
      setImgToDisplay(result);
    }
    loadImages();
  }, [img]);

  return (
    <div className="container-fluid footer">
      <div className="container">
        <div className="row no-gutters">
          <div className="col-12 col-lg-4">
            <img alt="footerImg" src={imgToDisplay} />
            <p>
              Hendaya 60, piso 13. Las Condes. Santiago. Chile <br />
              <a alt="" href="tel:+56934515690">
                +56934515690
              </a>
            </p>
          </div>
          <div className="col-12 col-lg-8 f-index">
            <ul>
            <div className="titlesFooter">Redes sociales</div>
              <li>
                <a
                  alt=""
                  href="https://www.facebook.com/23people"
                  target="blank"
                >
                  <i className="fab fa-facebook-square"></i> Facebook
                </a>
              </li>
              {/* <li>
                <a alt="" href="/">
                  <i className="fab fa-twitter"></i> Twitter
                </a>
              </li> */}
              <li>
                <a
                  alt=""
                  href="https://www.instagram.com/23people.io/"
                  target="blank"
                >
                  <i className="fab fa-instagram"></i> Instagram
                </a>
              </li>
              <li>
                <a
                  alt=""
                  href="https://www.linkedin.com/company/23people/"
                  target="blank"
                >
                  <i className="fab fa-linkedin"></i> LinkedIn
                </a>
              </li>
            </ul>
            <ul>
              <div className="titlesFooter">23people</div>
              <li>
                <a alt="" href="/">
                  Home
                </a>
              </li>
              <li>
                <a alt="" href="partners">
                  Partners
                </a>
              </li>
              {/* <li>
                <a alt="" href="talento">
                  Talento
                </a>
              </li>*/}
              <li>
                <a alt="" href="postulantes">
                  Únete a 23people
                </a>
              </li>
              <li>
                <a alt="" href="hablemos">
                  Hablemos
                </a>
              </li>
            </ul>
            {/* <ul>
              <h1>Servicios</h1>
              <li>
                <a alt="" href="soluciones">
                  23people Solutions
                </a>
              </li>
              <li>
                <a alt="" href="talento">
                  23people Talent
                </a>
              </li>
            </ul> */}
            <ul>
            <div className="titlesFooter">Postulantes</div>
              <li>
                <a alt="" href="postulantes">
                  Cultura 23people
                </a>
              </li>
              <li>
                <a
                  alt=""
                  href="https://www.emeral.cl/c/23people/career"
                  target="blank_"
                >
                  Ver postulaciones
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
