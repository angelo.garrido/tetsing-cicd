import React, { useState } from "react";
import { Link } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { useHistory } from "react-router-dom";

function TopBar(props) {
  let history = useHistory();
  const [isChecked, setIsChecked] = useState(false);
  function goHome() {
    if (isChecked) {
      history.push("/");
      setIsChecked(!isChecked);
    }
  }
  return (
    <nav>
      <Link to="/" className="logo" onClick={goHome} />
      <input
        type="checkbox"
        id="menu"
        onChange={(event) => setIsChecked(event.currentTarget.checked)}
        checked={isChecked}
      />
      <label htmlFor="menu" onClick={() => setIsChecked(isChecked)}>
        <div className="menu-bar"></div>
        <div className="menu-bar"></div>
        <div className="menu-bar"></div>
        <div className="menu-bar"></div>
      </label>
      <ul>
      <li>
          <NavLink
            to="postula"
            strict
            activeClassName="activeLink"
            onClick={() => setIsChecked(!isChecked)}
          >
            Postula
          </NavLink>
          {/* <a href="https://www.chiletrabajos.cl/encuentra-un-empleo?8=23people" >Postula</a> */}
        </li>
        <li>
          <NavLink
            to="postulantes"
            activeClassName="activeLink"
            onClick={() => setIsChecked(!isChecked)}
          >
            Cultura
          </NavLink>
        </li>
        <li>
          <NavLink
            to="equipo"
            activeClassName="activeLink"
            onClick={() => setIsChecked(!isChecked)}
          >
            Equipo
          </NavLink>
        </li>
        <li>
          <NavLink
            to="partners"
            activeClassName="activeLink"
            onClick={() => setIsChecked(!isChecked)}
          >
            Partners
          </NavLink>
        </li>
        {/*<li>
          <NavLink
            to="talento"
            activeClassName="activeLink"
            onClick={() => setIsChecked(!isChecked)}
          >
            Talento
          </NavLink>
        </li>
        <li>
          <NavLink
            to="blog"
            activeClassName="activeLink"
            onClick={() => setIsChecked(!isChecked)}
          >
            Blog
          </NavLink>
        </li>*/}
        
        <li>
          <Link
            to="hablemos"
            className="talk"
            onClick={() => setIsChecked(!isChecked)}
          >
            Hablemos
          </Link>
        </li>
      </ul>
    </nav>
  );
}

export default TopBar;
