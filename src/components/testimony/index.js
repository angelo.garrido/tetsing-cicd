import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { loadSingleImage } from "../../utils/imageLoader";
const Testimony = ({
  className,
  cardTitle,
  body,
  img,
  altImg,
  personName,
  personPosition,
  imgClass = "",
  hasButton,
  button,
}) => {
  let history = useHistory();

  const [imgToDisplay, setImgToDisplay] = useState(null);
  useEffect(() => {
    async function loadImages() {
      const result = await loadSingleImage(img);
      setImgToDisplay(result);
    }
    loadImages();
  }, [img]);

  return (
    <div className={className}>
      <div className="container">
        {cardTitle ? <h1>{cardTitle}</h1> : null}
        <div className="row no-gutters testimonies">
          {imgToDisplay ? (
            <div className="col-12 col-md-6 col-lg-4">
              <div className={imgClass}>
                <img alt={altImg} src={imgToDisplay} />
              </div>
            </div>
          ) : null}
          <div className="col-12 col-md-6 col-lg-8">
            <blockquote>{`«${body}»`}</blockquote>
            <div className="name-quote">
              <span>{personName} </span> {personPosition}
            </div>
            {hasButton ? (
            <div className={button.className}>
              <button
                onClick={() =>
                  button.isExternalLink
                    ? (window.location.href = button.url)
                    : history.push(button.url)
                }
              >
                {button.text}
              </button>
              
            </div>
          
                  ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Testimony;
