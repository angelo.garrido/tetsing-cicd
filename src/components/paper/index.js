import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

import Customers from "../customers";
import { loadSingleImage } from "../../utils/imageLoader";

const CardMaker = ({
  containerClass,
  textColumn,
  imgColumn,
  cardTitle,
  subContainerClass = "container",
  rowClass = "row no-gutters template",
}) => {
  let history = useHistory();

  const [imgToDisplay, setImgToDisplay] = useState(null);
  const isArrayOfImages = Array.isArray(imgColumn.img);
  useEffect(() => {
    async function loadImages() {
      const result = await loadSingleImage(imgColumn.img);
      setImgToDisplay(result);
    }
    if (!isArrayOfImages) {
      loadImages();
    }
  }, [imgColumn.img, isArrayOfImages]);

  return (
    <div className={containerClass}>
      <div className={subContainerClass}>
        {cardTitle ? <h1>{cardTitle}</h1> : null}
        <div className={rowClass}>
          <div className={textColumn.class}>
            {textColumn.title ? <h1>{textColumn.title}</h1> : null}
            {textColumn.body ? <p>{textColumn.body}</p> : null}
            {textColumn.hasButton ? (
              <div className={textColumn.button.className}>
                <button 
                  onClick={() =>
                    textColumn.button.isExternalLink
                      ? (window.location.href = textColumn.button.url)
                      : history.push(textColumn.button.url)
                  }
                >
                  {textColumn.button.text}
                </button>
              </div>
            ) : null}
          </div>

          <div className={imgColumn.class}>
            {isArrayOfImages ? (
              <Customers images={imgColumn.img} altImg={imgColumn.altImg} />
            ) : (
              <img alt={imgColumn.altImg} src={imgToDisplay} />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardMaker;
