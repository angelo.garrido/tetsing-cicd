const validator = (value) => (minLength, maxLength, regexToUse) => {
  return value.length >= minLength && value.length <= maxLength && regexToUse.test(value);
};

const REGEX_FULLNAME = /(^[a-z0-9aàáâãäåeèéêëiìíîïoòóôõöuùúûünñç]+([- '’]?[a-z0-9aàáâãäåeèéêëiìíîïoòóôõöuùúûünñç]+)*$|^$)/i;

const maxLengthFullname = 70;
const minLengthFullname = 0;

const isFullName = (value) => {
  return validator(value)(minLengthFullname, maxLengthFullname, REGEX_FULLNAME);
};

export default isFullName;
