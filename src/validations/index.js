export { default as isEmail }  from './emailValidator';
export { default as isFullName }  from './fullNameValidator';
export { default as isChileanTelephone }  from './phoneValidator';