const CHILEAN_TELEPHONE = /(^(\+?56|\(\+?56\)|\+?\(56\))?[- ]?(9[ -]?\d{4}|44[ -]?\d{3}|2[ -]?\d{4}|(?!44)[3-7][0-9][ -]?\d{2})[ -]?\d{4}$)/;

const isChileanTelephone = (value) => CHILEAN_TELEPHONE.test(value);

export default isChileanTelephone;
