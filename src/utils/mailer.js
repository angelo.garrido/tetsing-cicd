const axios = require('axios');

const sendMail = async msg => {
  try {
    const { REACT_APP_API_URL } = process.env;
    let success = true;
    await axios({
      url: `${REACT_APP_API_URL}/api/v1/mail`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Headers':
          'Origin, Content-Type, Accept, Authorization, X-Request-With'
      },
      data: JSON.stringify(msg)
    }).catch(err => {
      success = false;
    });

    return { success };
  } catch (error) {
    return { success: false };
  }
};
export default sendMail;
