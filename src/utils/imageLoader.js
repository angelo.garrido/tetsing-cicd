const loadArrayOfImages = async (images) => {
  let arrayOfImages = [];
  if(images && images.length){
    for await (let img of images) {
      await import(`../images/${img}`).then((image) =>
        arrayOfImages.push(image.default)
      );
    }
  }

  return arrayOfImages;
};

const loadSingleImage = async (img) => {
  let singleImage = null;
  if (img) {
    await import(`../images/${img}`)
      .then((image) => (singleImage = image.default))
      .catch((err) => console.log("Error loadSingleImage", err));
  }

  return singleImage;
};

export { loadArrayOfImages, loadSingleImage };
