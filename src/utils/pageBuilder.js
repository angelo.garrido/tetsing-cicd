import * as Components from '../components';

const build = array => {
  return array.map(({ componentType, ...props }) => ({
    Component: Components[componentType],
    props
  }));
};

export default { build };
