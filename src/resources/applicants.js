import React from "react";

export default [
  {
    componentType: "Testimony",
    className: "container-fluid testi23p",
    cardTitle: null,
    body:
      "Queremos crear una empresa en donde a nosotros mismos nos hubiera gustado trabajar",
    img: "testi5.jpg",
    altImg:"Testimonio 5",
    personName: <>Equipo fundador de 23people.</>,
    personPosition: null,
    hasButton: true,
      button: {
        className: "conocenos-Button buttons-actions",
        isExternalLink: false,
        url: "/equipo",
        text: "Conócenos",
    
        
      },
  },
  {
    componentType: "Paper",
    containerClass: "container-fluid work blue3",
    subContainerClass: "",
    rowClass: "row no-gutters",
    cardTitle: "",
    textColumn: {
      class: "col-12 col-md-11 col-lg-5 col-xl-4 offset-md-1 offset-lg-2 ",
      title:
        "Unidos a pesar de la distancia ¡Todos nuestros objetivos han sido logrados!",
      body:
        "Nuestro objetivo al igual que tu objetivo es ser feliz trabajando y haciendo aquello que te apasiona en un ambiente de trabajo donde puedas expresar tus opiniones, crecer profesional y personalmente. We are in remoto mood.",
      hasButton: true,
      button: {
        isExternalLink: true,
        url: "https://hiring.23people.io",
        text: "Postula",
        className: "buttons-actions",

      },
    },
    imgColumn: {
      img: "foto_home_c.jpg",
      altImg:"Foto Home",
      class: "col-12 col-md-12 col-lg-4 col-xl-4 team-img",
    },
  },
  {
    componentType: "MultiCards",
    title: "El  miembro más importante del equipo eres tú. Pero no es pura charla: ¡Lo demostramos!",
    className: "container-fluid template-wrapper cases",
    rowSubClass: "row no-gutters col-12 col-md-10 offset-md-1",
    cards: [
      {
        className: "col-12 col-md-4 case-card",
        subTitle: "Mejoras de espacio de trabajo",
        body: null,
        img: "incentivos/escritorios.gif",
        altImg:"Escritorios"
      },
      {
        className: "col-12 col-md-4 case-card",
        subTitle: "Kit de Bienvenida",
        body: "",
        img: "incentivos/kit-bienvenida.gif",
        altImg:"Kit de Bienvenida"
      },
      {
        className: "col-12 col-md-4 case-card",
        subTitle: "E-nicitivas",
        body: "",
        img: "incentivos/e-afteroffice.gif",
        altImg:"After Office"
      },
    ],
  },
  
  {
    componentType: "Carousel",
    id: "culture",
    body: [
      {
        componentType: "MultiCards",
        title: "Un gran poder, conlleva una gran responsabilidad (con nuestros talentos)",
        className: "container-fluid template-wrapper cases",
        rowSubClass: "row no-gutters col-12 col-md-10 offset-md-1",
        anclaClass: "Beneficios",
        cards: [
          {
            className: "col-12 col-md-6 col-lg-3 case-card",
            subTitle: "Reembolsos por certificaciones",
            body:
              "Podemos reembolsarte gran parte de tu certificación internacional que realices.",
            img: "beneficios/Bene1.jpg",
            altImg:"Reembolso"
          },
          {
            className: "col-12 col-md-6 col-lg-3 case-card",
            subTitle: "Bono por vacaciones",
            body: "Te pagamos por cada semana de vacaciones que te tomes. (Si, has leído bien).",
            img: "beneficios/Bene2.jpg",
            altImg:"Bono vacaciones"
          },
          {
            className: "col-12 col-md-6 col-lg-3 case-card",
            subTitle: "Bono Baby Shower",
            body: "Luego de tu primer año con nosotros, si tienes la alegría de ser un futur@ madre (o padre), se te entregará un bono de parte de todo el equipo como obsequio para cuando comience el prenatal.",
            img: "beneficios/Bene3.jpg",
            altImg:"Bono Baby Shower"
          },
          {
            className: "col-12 col-md-6 col-lg-3 case-card",
            subTitle: "Regalo especial de cumpleaños",
            body:
              "Cuando sea tu cumpleaños, buscaremos un regalo especial y entretenido, especialmente elegido para tí.",
            img: "beneficios/Bene4.jpg",
            altImg:"Regalo especial de cumpleaños"
          },
          {
            className: "col-12 col-md-6 col-lg-3 case-card",
            subTitle: "Seguro dental y de salud complementario",
            body:
              "A través de Metlife, proporcionamos no solamente un seguro complementario de salud y sino que también dental.",
              img: "beneficios/Bene5.jpg",
              altImg:"Seguro Dental Complementario"
          },
          {
            className: "col-12 col-md-6 col-lg-3 case-card",
            subTitle: "Biblioteca de cursos online",
            body:
              "Tenemos una biblioteca de cursos online totalmente gratuitos para todos nuestros colaboradores en Udemy, Pero, si te interesa otro curso incluso en otra plataforma, puedes solicitarlo.",
              img: "beneficios/Bene6.jpg",
              altImg:"Cursos Online"
          },
          {
            className: "col-12 col-md-6 col-lg-3 case-card",
            subTitle: "Seguro COVID-19 para todos",
            body:
              "Proveemos seguro Covid-19 para cuidarnos todos (solo es obligatorio para empresas con trabajo presencial, pero nosotros nos cuidamos entre todos).",
            img: "beneficios/Bene7.jpg",
            altImg:"Seguro Covid"
          },
          {
            className: "col-12 col-md-6 col-lg-3 case-card",
            subTitle: "Iniciativa para vacunación influenza",
            body:
              "Entregamos un presupuesto de $10.000.- para quienes quieran vacunarse contra la influenza.",
            img: "beneficios/Bene8.jpg",
            altImg:"Influenza"
          },
          {
            className: "col-12 col-md-6 col-lg-3 case-card",
            subTitle: "Convenio Banco de Chile",
            body:
              "Cuando tengas tu primera liquidación de sueldo, podrás solicitar una cuenta corriente en el Banco de Chile.",
            img: "beneficios/Bene9.jpg",
            altImg:"Banco Chile"
          },
        ],
      },
    ],
  },
  {
    componentType: "MultiCards",
    title: "Lo que nuestros jóvenes aprendices dicen de nosotros",
    dropTitle: "(Disclaimer: La fuerza no ha sido utilizada para influenciar la opinión de ningún participante)",
    className: "container-fluid clients clients-icons pink-applicants",
    rowSubClass:
      "row no-gutters col-12 col-md-8 offset-md-2 justify-content-md-center",
    cards: [],
  },


  {
    componentType: "Carousel",
    id: "testimony",
    body: [
      {
        componentType: "Testimony",
        wrapperClass: "testimonies-slide carousel-item testi-green active",
        cardTitle: null,
        body:
          "Mi estadía ha sido muy agradable, se nota un clima muy ameno y una preocupación real por nosotros. Por lo que me ha permitido saltar a mi primer liderazgo, lo que estoy muy agradecido. Junto con conocer grandes profesionales. Demuestran su cariño y real preocupación por su gente. ¡Gracias por todas las instancias agradables que realizan para regalonearnos!.",
        img: "joaquin_salazar_TL_API_c.jpg",
        altImg:"Joaquin Salazar",
        imgClass: "testi-img",
        personName: "Joaquín Salazar,",
        personPosition: "Technical Lead. API. Equifax Estados Unidos.",
      },
      {
        componentType: "Testimony",
        wrapperClass: "testimonies-slide carousel-item testi-blue",
        cardTitle: null,
        body:
          "Me he sentido muy bien en la estadía en la empresa, me ha aportado ser parte de un equipo en mi desarrollo y mejora en manejo de inglés. Lo que más destaco son sus valores, su manejo hacia las personas y su apoyo.",
        img: "camila_ramirez_c.jpg",
        altImg:"Camila Ramirez",
        imgClass: "testi-img",
        personName: "Camila Ramirez,",
        personPosition: "Test Engineer. API. Equifax Estados Unidos.",
      },
      {
        componentType: "Testimony",
        wrapperClass: "testimonies-slide carousel-item testi-orange",
        cardTitle: null,
        body:
          "Me he sentido bastante bien. El ambiente de trabajo es super cómodo y los compañeros de trabajo son lo máximo. Sin duda una de las mejores empresas donde he estado. Hasta el momento he aprendido muchas cosas nuevas (Herramientas y Frameworks) y sobre todo estoy perfeccionando mi nivel de inglés con el cliente Equifax. Es una empresa en donde se puede apreciar bastante la unión de todos los equipos de trabajo. Me gustaría felicitarlos por la metodología de trabajo tan unida que tienen.",
        img: "david_azuaje_c.jpg",
        altImg:"David Azuaje",
        imgClass: "testi-img",
        personName: "David Azuaje,",
        personPosition: "Test Engineer. API. Equifax Estados Unidos.",
      },
      {
        componentType: "Testimony",
        wrapperClass: "testimonies-slide carousel-item testi-green",
        cardTitle: null,
        body:
          "Lo que me invitó a aplicar a 23people es la preocupación por el trabajador, normalmente en otras empresas no es así y aqui se nota la diferencia. Otras grandes ventajas son la participación activa en proyectos y los beneficios! Tu trabajo y tu talento son reconocidos y recompensados pero a la vez es un ambiente flexible y amigable desde la postulación. Trabajo por tranquilidad porque aún si surjen inconvenientes, se que cuento con el apoyo  23people para solucionarlos y acompañar cada proceso.",
        img: "eliseo_henriquez_c.jpeg",
        altImg:"Eliseo Henriquez",
        imgClass: "testi-img",
        personName: "Eliseo Henriquez,",
        personPosition: "Desarrollador Sr",
      },
      {
        componentType: "Testimony",
        wrapperClass: "testimonies-slide carousel-item testi-blue",
        cardTitle: null,
        body:
          "Me interesé por postulas en 23people porque se veía como una empresa muchísimo más cercana y se sentía como una empresa joven. Ahora, siendo parte, me gusta no percibir burocracia y que haya flexibildiad: se escuchan tus iniciativas y planteamientos. 23people ve el potencial en el profesional, esto le preocupa más que un checklist que tiene que cumplir, 23people analiza todas las variables. Es como un respiro a las empresas típicas y corporativas. Es una empresa con mucha gente joven, mucha flexibilidad, entendimiento de parte tus compañeros. Por ejemplo, me encanta el ambiente: todos compartimos la pasión por la tecnología, siento que todos hablamos el mismo idioma.",
        img: "claudio_moran_c.jpeg",
        altImg:"Claudio Moran",
        imgClass: "testi-img",
        personName: "Claudio Moran,",
        personPosition: "Desarrollador de software",
      },
    ],
  },
  {
    componentType: "Contact",
    firstColumn: {
      className: "col-12 col-md-6 cs-card cs-orange",
      title: "Súmate a nuestro equipo",
      button: {
        text: "Postula",
        url: "https://hiring.23people.io",
        isExternalLink: true,
      },
    },
    secondColumn: {
      className: "col-12 col-md-6 cs-card cs-black",
      title: "¿Tienes alguna duda?",
      button: { text: "Hablemos", url: "/hablemos" },
    },
  },
  /*{
    componentType: "Hero",
    className: "container-fluid home-doublecard",
    firstColumn: {
      className: "col-12 col-md-6 col-sm-12 triple-card green",
      title: <>¡Te queremos en nuestro equipo!</>,
      body:
        "Si a ti te va bien: a nosotros nos va bien. Es así de simple. Por eso, te acompañaremos en todo el proceso de selección y todo nuestro esfuerzo estará concentrado en que puedas desarrollar tu potencial en un ambiente de aprendizaje y crecimiento continuo (Psst ... Chequea también nuestros paquetes de beneficios)",
        buttons:[
          { url: "/hablemos", 
            text: "Postula!", 
            className: "postula-Button buttons-actions"}, 
          { url: "/hablemos",  
            text: "Beneficios",
            className: "beneficios-Button buttons-actions"}, 
          { url: "/hablemos", 
            text: "Testimonios",
            className: "testimonios-Button buttons-actions" }
        ],  
    },
    secondColumn: {
      className: "col-12 col-md-6 col-sm-12 triple-card violet",
      title: "¿Buscas formar un equipo Ágil?",
      body:
        "Construimos equipos de profesionales TI preparados para adaptarse a cualquier desafío según tus necesidades. Pero no nos conformamos solo con eso: nos ocupamos de hacer seguimiento, mentorías y rectificaciones ante cualquier inconveniente que pueda presentarse. No dejamos nada al azar. ",
        buttons:
        [
          { 
            url: "/hablemos", 
            text: "Hablemos" ,
            className: "hablemos-Button buttons-actions"

          }
        ],
      },
  },*/
  
  
];
