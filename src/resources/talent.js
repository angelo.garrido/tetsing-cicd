import React from "react";

export default [
  {
    componentType: "Paper",
    containerClass: "container-fluid home talent",
    rowClass: "row no-gutters services",
    subContainerClass: "",
    cardTitle: null,
    textColumn: {
      class: "col-12 col-md-6 col-lg-4 order-1 offset-md-1 serv-title",
      title: "Formación de equipos ágiles",
      body:
        "Construimos equipos en base a la adaptabilidad con capacidad para cualquier desafío según tus necesidades. Construimos  equipos de profesionales TI  con la capacidad de adaptabilidad para cualquier desafío según tu necesidad.",
      hasButton: false,
    },
    imgColumn: {
      img: "talent.png",
      altImg:"Foto Talent",
      class: "col-12 col-md-5 col-lg-7 col-xl-7 order-2 serv-img",
    },
  },

  {
    componentType: "Hero",
    className: "container-fluid home-doublecard",
    firstColumn: {
      className: "col-12 col-md-6 col-sm-12 triple-card green",
      title: <>Formación de equipos ágiles</>,
      body:
        "Construimos equipos en base a la adaptabilidad con capacidad para cualquier desafío según tus necesidades. Construimos  equipos de profesionales TI  con la capacidad de adaptabilidad para cualquier desafío según tu necesidad.",
        buttons:[
          { url: "/hablemos", 
            text: "Hablemos", 
            className: "postula-Button buttons-actions"}, 
        ],  
    },
    secondColumn: {
      className: "col-12 col-md-6 col-sm-12 triple-card violet",
      title: "Reclutamiento de profesionales TI",
      body:
        "Nuestras soluciones van acorde a tus inquietudes, y gracias a esto hemos logrado generar una confianza con nuestros candidatos que se ve reflejada en nuestro proceso de selección a lo largo del tiempo.",
        buttons:
        [
          { 
            url: "/hablemos", 
            text: "Hablemos" ,
            className: "hablemos-Button buttons-actions"

          }
        ],
      },
  },
  {
    componentType: "Testimony",
    className: "container-fluid white1 testi-alone",
    cardTitle: null,
    body:
      "Es una empresa que presta servicios de alta calidad. Las personas seleccionadas tienen alto nivel profesional y tienen el perfil que necesitamos. Adicionalmente, se preocupan por el bienestar de los colaboradores, y esto es tangible en beneficios, se aseguran de que tengan un buen ambiente de trabajo, están en permanente comunicación con ellos lo que los hace muy cercanos a los colaboradores. La empresa tiene procesos eficientes, son responsables y cumplidores de las normas.",
    img: "testi4.jpg",
    personName: "Raquel Guerrero,",
    personPosition:
      "Líder de proyecto D&A Statistical Specialist. Equifax. Chile.",
  },
  {
    componentType: "Paper",
    containerClass: "container-fluid brown",
    subContainerClass: "container",
    rowClass: "row no-gutters serv-subtitle",
    cardTitle: null,
    textColumn: {
      class: "col",
      title:
        "¡Voilá! Enfócate en tus objetivos y nosotros nos encargamos del resto.",
      body: null,
      hasButton: true,
      button: {
        className: "buttons-actions",
        url: "/hablemos",
        text: "Hablemos",
      },
    },
    imgColumn: {
      img: "",
      class: "",
    },
  },
];
