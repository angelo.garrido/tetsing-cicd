import React from "react";


export default [
 
  {
    componentType: "Paper",
    containerClass: "container-fluid home solutions",
    rowClass: "row no-gutters services",
    subContainerClass: "",
    cardTitle: null,
    textColumn: {
      class: "col-12 col-md-7 col-lg-4 order-1 order-md-2 serv-title",
      title: "Nosotros",
      body:
        " Nosotros Nosotros Nosotros Nosotros",
      seeMore: false,
    },
    imgColumn: {
      img: "solutions.png",
      altImg:"Foto Solutions",
      class: "col-12 col-md-5 col-lg-8 col-xl-7 order-2 order-md-1 serv-img",
    },
  },

  {
    componentType: "Paper",
    containerClass: "container-fluid",
    rowClass: "row no-gutters serv-subtitle white1",
    cardTitle: null,
    textColumn: {
      class: "col",
      title: <>Con herramientas tecnológicas de alto nivel y seguridad</>,
      body: null,
      seeMore: false,
    },
    imgColumn: {
      img: "",
      class: "",
    },
  },
  {
    componentType: "Paper",
    containerClass: "container-fluid clients ",
    subContainerClass: "",
    cardTitle: null,
    rowClass: "row no-gutters",
    textColumn: {
      class: "",
      title: "",
      body: "",
    },
    imgColumn: {
      img: [
        "languages/big-data.jpg",
        "languages/ws.jpg",
        "languages/bd.jpg",
        "languages/frontend.jpg",
        "languages/cloud.jpg",
        "languages/mobile.jpg",
      ],
      altImg:"Foto Solutions 2",
      class: "col client-list white-clients",
    },
  },

];
