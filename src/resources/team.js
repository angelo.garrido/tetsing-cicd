export default [
  
  {
    componentType: "MultiCardsHover",
    title: "Diferentes visiones producen grandes ideas",
    className: "container-fluid template-wrapper cases",
    rowSubClass: "row no-gutters col-12 col-md-10 offset-md-1",
    cards: [
      {
        className: "col-6 col-md-6 col-lg-3 case-card",
        subTitle: "Belen Alarcón",
        body: "People Care Manager",
        img: "team/belen-alarcon.jpg",
        subClassName: "containerOverlay",
        secondSubClassName: "overlay",
        link: "https://www.linkedin.com/in/bel%C3%A9n-alarc%C3%B3n-maureira-4864b5bb/",
        linkClassName: "fab fa-linkedin",
        altImg:"Belen Alarcon"
      },
      {
        className: "col-6 col-md-6 col-lg-3 case-card",
        subTitle: "José Centeno",
        body: "Reclutamiento y Selección",
        img: "team/jose-centeno.jpg",
        subClassName: "containerOverlay",
        secondSubClassName: "overlay",
        link: "https://www.linkedin.com/in/jos%C3%A9-centeno-ordo%C3%B1ez-a05780165/",
        linkClassName: "fab fa-linkedin",
        altImg:"Jose Centeno"
      },
      {
        className: "col-6 col-md-6 col-lg-3 case-card",
        subTitle: "Anita Villegas",
        body: "Comercial, Reclutamiento y Selección",
        img: "team/anitaVillegas.png",
        subClassName: "containerOverlay",
        secondSubClassName: "overlay",
        link: "https://www.linkedin.com/in/anita-villegas/",
        linkClassName: "fab fa-linkedin",
        altImg:"Anita Villegas"
      },
      {
        className: "col-6 col-md-6 col-lg-3 case-card",
        subTitle: "Carolina Muñoz",
        body: "Producto",
        img: "team/carolinaMunoz.png",
        subClassName: "containerOverlay",
        secondSubClassName: "overlay",
        link: "https://www.linkedin.com/in/carolina-munoz-ceron/",
        linkClassName: "fab fa-linkedin",
        altImg:"Carolina Muñoz"
      },
      {
        className: "col-6 col-md-6 col-lg-3 case-card",
        subTitle: "Gisella Donoso",
        body: "Desarrollo",
        img: "team/giselaDonoso.png",
        subClassName: "containerOverlay",
        secondSubClassName: "overlay",
        link: "https://www.linkedin.com/in/gisela-donoso-polloni/",
        linkClassName: "fab fa-linkedin",
        altImg:"Gisela Donoso"
      },
      {
        className: "col-6 col-md-6 col-lg-3 case-card",
        subTitle: "Claudio Morán",
        body: "Desarrollo",
        img: "team/claudioMoran.png",
        subClassName: "containerOverlay",
        secondSubClassName: "overlay",
        link: "https://www.linkedin.com/in/claudio-moran-079b46106/",
        linkClassName: "fab fa-linkedin",
        altImg:"Claudio Moran"
      },
      {
        className: "col-6 col-md-6 col-lg-3 case-card",
        subTitle: "Edison Acuña",
        body: "Comercial",
        img: "hablemos/EdisonAcuna.png",
        subClassName: "containerOverlay",
        secondSubClassName: "overlay",
        link: "https://www.linkedin.com/in/edisona1/",
        linkClassName: "fab fa-linkedin",
        altImg:"Edison Acuña"
      },
      {
        className: "col-6 col-md-6 col-lg-3 case-card",
        subTitle: "Catalina Paz Stange",
        body: "Reclutamiento y Selección",
        img: "hablemos/CatalinaStange.png",
        subClassName: "containerOverlay",
        secondSubClassName: "overlay",
        link: "https://www.linkedin.com/in/catalinapazstangesalinas/",
        linkClassName: "fab fa-linkedin",
        altImg:"Edison Acuña"
      },
    ],
  },
  
];
