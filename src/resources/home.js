//import { color } from "@material-ui/system";
import React from "react";
import { MultiColorText } from "../components";

const colors = [
  { color: "#26A9E0", text: "Partners" },
  { color: "#f15c30", text: "Tecnología" },
  { color: "#2d845e", text: "Adaptabilidad" },
  { color: "#dd3d4b", text: "Mejora continua" },
];

//const color2 =  { color: "#26A9E0", text: "¡Postula!" ,url: "/hablemos"};

 /*const colors3 = [
  { colorA: "#26A9E0", text: "Hablemos" },
  { colorB: "#f15c30", text: "Programa yoda?" },
 ];*/


export default [
 {
    componentType: "Carousel",
    id: "home",
    body: [
      /*{
        componentType: "Paper",
        containerClass: "container",
        rowClass: "row no-gutters",
        wrapperClass: "carousel-item ",
        cardTitle: "",
        textColumn: {
          class: "col-12 col-md-6 col-xl-6 order-1 order-md-1",
          title: (
            <>
              ¡Hola! Aterrizaste en <MultiColorText wordsWithColors={colors} />
            </>
          ),
          body: (
            <>
              Tus desafíos se transforman en los nuestros, nos preocupamos por
              entregar valor continuamente para que notes que tu inversión con
              nosotros va por buen camino. ¡Construyamos juntos!
            </>
          ),
          hasButton: true,
          button: {
            className: "buttons-actions",
            url: "/partners",
            text: "Conoce +",
          },
        },
        imgColumn: {
          img: "animacion4.gif",
          altImg:"Foto Home 1",
          class: "col-12 col-md-6 col-xl-6 order-2 order-md-2",
        },
      },*/
        /*{
        componentType: "Paper",
        containerClass: "container",
        rowClass: "row no-gutters",
        wrapperClass: "carousel-item blue active",
        cardTitle: "",
        textColumn: {
          class: "col-12 col-md-6 col-xl-6 order-1 order-md-1",
          title: "23people te ha enviado una solicitud de amistad",
          body:
            "¡Entra! En nuestra casa, las luces siempre están prendidas. Elevator's pitch: Seremos el R2 de tu Luke, para que solamente tengas que ocuparte de darle al target.   ¿Te apuntas?",
          hasButton: true,
          button: {
            className: "buttons-actions",
            url: "/postulantes",
            text: "Súmate",
          },
        },
        imgColumn: {
          img: "hands.jpg",
          altImg:"Foto Home 2",
          class: "col-12 col-md-6 col-xl-6 order-2 order-md-2",
        },
      },*/
      {
        componentType: "Paper",
        containerClass: "container",
        rowClass: "row no-gutters",
        wrapperClass: "carousel-item active",
        cardTitle: "",
        textColumn: {
          class: "col-12 col-md-6 col-xl-6 order-1 order-md-1",
          title: (
            <>
              ¡Hola! Aterrizaste en <MultiColorText wordsWithColors={colors} />
            </>
          ),
          body: (
            <>
              Adherimos plenamente a los principios de la agilidad: respeto a las personas, mejora continua y excelencia técnica.
Buscamos establecer relaciones de largo plazo con nuestros clientes,  en las cuales exista respeto mutuo y colaboración permanente que aporten valor estratégico.
            </>
          ),
          hasButton: true,
          button: {
            className: "buttons-actions",
            url: "/partners",
            text: "Conoce +",
          },
        },
        imgColumn: {
          img: "animacion4.gif",
          altImg:"Foto Home 1",
          class: "col-12 col-md-6 col-xl-6 order-2 order-md-2",
        },
      },
    ],
  },
  {
    componentType: "BasicModal",
    className: "container",  
      title: "¡Estamos contigo!",
      body: "Siempre hemos estado cerca de nuestros clientes, ayudándolos a resolver desafíos tecnológicos, de obtención de talento TI y estratégicos que involucran al elemento humano. COVID-19 no es la excepción. ¡Seguimos estando presentes!",
      img: "home/conectlogo.png",
      classImg:"connectLogo",
      altImg:"Logo Conectados"
       
  },
   
  {
    componentType: "Hero",
    className: "container-fluid home-doublecard",
    firstColumn: {
      className: "col-12 col-md-4 col-sm-12 triple-card green",
      title: <>Matcheamos  a developers con empresas que los necesitan</>,
      body:
        "Nos dedicamos a buscar developers para nuestros clientes. Te acompañamos en todo el proceso de selección y todo nuestro esfuerzo estará concentrado en que puedas desarrollar tu potencial en un ambiente de aprendizaje y crecimiento continuo (Psst...chequea también nuestros beneficios).",
        buttons:[
          { url: "https://www.chiletrabajos.cl/encuentra-un-empleo?8=23people", 
            text: "Postula!", 
            className: "postula-Button buttons-actions",
            isExternalLink: true
          }, 
          { url: "/postulantes#Beneficios",  
            text: "Beneficios",
            className: "beneficios-Button buttons-actions",
            isExternalLink: true
          }, 
          { url: "/partners", 
            text: "Testimonios",
            className: "testimonios-Button buttons-actions",
            isExternalLink: false
          }
        ],  
    },
    secondColumn: {
      className: "col-12 col-md-4 col-sm-12 triple-card violet",
      title: "¿Buscas formar un equipo Ágil?",
      body:
        "Construimos equipos de profesionales TI preparados para ayudar a nuestros clientes con su proceso de transformación digital. Pero no nos conformamos solo con eso: nos ocupamos de hacer seguimiento, mentorías y rectificaciones ante cualquier eventualidad que pueda presentarse. No dejamos nada al azar, cuidando tanto a clientes como a colaboradores. ",
        buttons:
        [
          { 
            url: "/hablemos", 
            text: "Hablemos" ,
            className: "hablemos-Button buttons-actions"

          }
        ],
      },
      thirdColumn: {
        className: "col-12 col-md-4 col-sm-12 triple-card green",
        title: <>Soluciones informáticas</>,
        body:
          "Creamos soluciones innovadoras para superar los desafíos de nuestros clientes, con nuestros equipos ágiles de desarrollo TI que están en constante capacitación para abordar desafíos en forma activa e iterativa.",
          buttons:[
            { 
              url: "/hablemos", 
              text: "Hablemos" ,
              className: "testimonios-Button buttons-actionsœ"
  
            }
          ],  
      },
  },
  {
    componentType: "MultiCards",
    title: null,
    cards: [
      {
        className: "col-12 col-md-3 graph",
        subTitle:
          "Nuestros profesionales cumplen con la expectativa de nuestros clientes el 80% de las ocasiones.",
        img: "Iconos.jpg",
        altImg:"Expectativas"
      },
      {
        className: "col-12 col-md-3 graph",
        subTitle: "Las rotaciones en equipos son menores al 3%.",
        img: "Iconos2.jpg",
        altImg:"Rotaciones Casi Nulas"
      },
      {
        className: "col-12 col-md-3 graph",
        subTitle: "Nuestros clientes nos recomiendan el 85% de las veces.",
        img: "Iconos3.jpg",
        altImg:"Nos Recomiendan"
      },
      {
        className: "col-12 col-md-3 graph",
        subTitle: "Juntos logramos tener presencia internacional",
        img: "Iconos4.jpg",
        altImg:"Mundo"

      },
    ],
  }, 
  /*{
    componentType: "MultiCards",
    title: "¡Recursos Free aquí!",
    dropTitle: "Ahora que tenemos tu atención...¡No, en serio! Son recursos valiosos y son gratis. De nada. Nos cuentas qué tal están ;)",
    cards: [
      {
        className: "col-12 col-md-4 graph",
        subTitle:
          "Laboratoria: nuestro impacto",
        img: "recursosFree/recursosFree01.png",
        altImg:"recursosFree01"
      },
      {
        className: "col-12 col-md-4 graph",
        subTitle: "Qué beneficios tiene aprender remoto en el bootcamp?",
        img: "recursosFree/recursosFree02.png",
        altImg:"recursosFree02"
      },
      {
        className: "col-12 col-md-4 graph",
        subTitle: "¿Cómo encontrar al mejor talento y cerrar la brecha de género en tecnología",
        img: "recursosFree/recursosFree03.png",
        altImg:"recursosFree03"
      },
    ],
  },
  {
    componentType: "Paper",
    containerClass: "container-fluid white home",
    subContainerClass: "container",
    rowClass: "row no-gutters serv-subtitle",
    cardTitle: null,
    textColumn: {
      class: "col",
      title:         "",
      body: null,
      seeMore: true,
      hasButton: true,
      button: {
        className: "yellow buttons-actions",
        isExternalLink: true,
        url: "https://blog.23people.io/",
        text: "Por favor, no entres a este link.",
    
        
      },
    },
    imgColumn: {
      img: "",  
      class: "",
    },
  }*/ 
 
   
];
