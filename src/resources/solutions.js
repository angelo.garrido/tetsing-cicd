export default [

  {
    componentType: "Carousel",
    id: "testimony",
    body: [
      {
        componentType: "Testimony",
        wrapperClass: "testimonies-slide carousel-item testi-green active",
        cardTitle: null,
        body:
          "Una de las cualidades que más valoro de trabajar con 23people es el gusto por el desafío, llevando a cabo los proyectos junto a nosotros. Además, entregando un trabajo bueno, diferencial y mejor a lo esperado.",
        img: "testimony-1.jpg",
        altImg:"José Pavez",
        imgClass: "testi-img",
        personName: "José Pavez,",
        personPosition: "Chief of Software Development Technological Services Management ACHS.",
      },
      {
        componentType: "Testimony",
        wrapperClass: "testimonies-slide carousel-item testi-blue",
        cardTitle: null,
        body:
          "“23people ha sido un verdadero partner para Wayra. El equipo de desarrollo de 23people se integró plenamente, entendió correctamente la problemática de negocios planteada, guió el desarrollo con una forma ágil de trabajo y construyó una excelente plataforma, muy intuitiva y de fácil uso, entregando valor desde la etapa más temprana Esperamos seguir trabajando con ellos en el futuro.",
        img: "testimony-2.jpg",
        altImg:"Sebastian Gonzalez",
        imgClass: "testi-img",
        personName: "Sebastian Gonzalez,",
        personPosition: "Head of Venture Partnerships, WAYRA.",
      }
    ],
  },
  {
    componentType: "MultiCards",
    title: "Nuestros clientes son la prueba del camino que hemos recorrido, creciendo y ganando experiencia junto a ellos: literalmente #TeamWork.",
    dropTitle: null,
    className: "container-fluid clients clients-icons blue-clients",
    rowSubClass:
      "row no-gutters col-12 col-md-8 offset-md-2 justify-content-md-center",
    cards: [],
  },

  {
    componentType: "MultiCards",
    title: "Casos de éxito que nos enorgullecen:",
    className: "container-fluid template-wrapper cases case-parther",
    cards: [
      {
        className: "col-12 col-md-6 col-lg-3 case-card",
        subTitle: null,
        body:
          "Desarrollamos la nueva versión de la plataforma para clientes de Platinum 360: el informe comercial más completo para validación y respaldo de antecedentes comerciales de una persona.",
        img: "cases/equifax1.jpg",
      },
      {
        className: "col-12 col-md-6 col-lg-3 case-card",
        subTitle: null,
        body:
          "El proyecto consistió en la implementación de una plataforma para conectar startups con grandes empresas que estén dispuestas a financiar proyectos.",
        img: "cases/wayra.jpg",
      },
      {
        className: "col-12 col-md-6 col-lg-3  case-card",
        subTitle: null,
        body:
          "Consistió en la implementación de un servicio SOAP que mantuviese el contrato del servicio actual, incluyendo códigos de retorno y que funcione como intermediario entre los servicios y el normalizador REST de Map City.",
        img: "cases/mcity.jpg",
      },
      {
        className: "col-12 col-md-6 col-lg-3  case-card",
        subTitle: null,
        body:
          "Desarrollamos una aplicación que permite ingresar de manera centralizada los pacientes de cada empresa que estén diagnosticados por COVID-19, permitiendo el seguimiento continuo de cada etapa del paciente hasta su recuperación.",
        img: "cases/achs.jpg",
      },
      {
        className: "col-12 col-md-6 col-lg-3  case-card",
        subTitle: null,
        body:
          "Se realizó apoyo en la definicion de los patrones de arquitectura cloud para las diversas plataformas y aplicaciones de la compañia.",
        img: "cases/sura.png",
      },
    ],
  },
  {
    componentType: "Paper",
    containerClass: "container-fluid green",
    subContainerClass: "container",
    rowClass: "row no-gutters serv-subtitle",
    cardTitle: null,
    textColumn: {
      class: "col",
      title:
        "¡Voilá! Enfócate en tus objetivos y nosotros nos encargamos del resto.",
      body: null,
      seeMore: true,
      hasButton: true,
      button: {
        className:"hablemos-partners-Button buttons-actions",
        url: "/hablemos",
        text: "Hablemos",
      },
    },
    imgColumn: {
      img: "",
      class: "",
    },
  },
];
