import React from "react";

export default [

  {
    componentType: "ContactForm"
  },


  {
    componentType: "MultiCards",
    title: "Conoce nuestro contenido",
    dropTitle: null,
    cards: [
      {
        className: "col-12 col-md-4 graph",
        subTitle:
          "Laboratoria: nuestro impacto",
        img: "recursosFree/recursosFree01.png",
        altImg:"recursosFree01"
      },
      {
        className: "col-12 col-md-4 graph",
        subTitle: "Qué beneficios tiene aprender remoto en el bootcamp?",
        img: "recursosFree/recursosFree02.png",
        altImg:"recursosFree02"
      },
      {
        className: "col-12 col-md-4 graph",
        subTitle: "¿Cómo encontrar al mejor talento y cerrar la brecha de género en tecnología?",
        img: "recursosFree/recursosFree03.png",
        altImg:"recursosFree03"
      },
    ],
  },


  {
    componentType: "MultiCards",
    title: "Conoce nuestro contenido",
    dropTitle: null,
    cards: [
      {
        className: "col-12 col-md-4 graph",
        subTitle:
          "Laboratoria: nuestro impacto",
        img: "recursosFree/recursosFree01.png",
        altImg:"recursosFree01"
      },
      {
        className: "col-12 col-md-4 graph",
        subTitle: "Qué beneficios tiene aprender remoto en el bootcamp?",
        img: "recursosFree/recursosFree02.png",
        altImg:"recursosFree02"
      },
      {
        className: "col-12 col-md-4 graph",
        subTitle: "¿Cómo encontrar al mejor talento y cerrar la brecha de género en tecnología?",
        img: "recursosFree/recursosFree03.png",
        altImg:"recursosFree03"
      },
    ],
  },
  {
    componentType: "Paper",
    containerClass: "container-fluid white",
    subContainerClass: "container",
    rowClass: "row no-gutters serv-subtitle",
    cardTitle: null,
    textColumn: {
      class: "col",
      title:         "",
      body: null,
      seeMore: true,
      hasButton: true,
      button: {
        className: "yellow buttons-actions",
        isExternalLink: true,
        url: "https://blog.23people.io/",
        text: "SIGUE NUESTRO HUB",
    
        
      },
    },
    imgColumn: {
      img: "",  
      class: "",
    },
  }, 
];
