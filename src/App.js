import React from "react";
import { ScrollToTop, TopBar, Footer } from "./components";
import { Home, Applicants, Solutions, Contact, Team, Apply } from "./modules";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <ScrollToTop />
      <TopBar />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/postulantes">
          <Applicants />
        </Route>
        <Route exact path="/equipo">
          <Team />
        </Route>
        <Route exact path="/partners">
          <Solutions />
        </Route>
        {/*<Route exact path="/talento">
          <Talent />
        </Route>
        <Route exact path="/blog">
          <Blog />
        </Route>*/}
        <Route exact path="/postula">
          <Apply />
        </Route>
        <Route exact path="/hablemos">
          <Contact />
        </Route>
        <Route path="*">
          <Home />
        </Route>
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
